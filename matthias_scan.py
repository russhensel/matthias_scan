# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 16:15:49 2020

@author: russ

fork of some code from smart_plug.py just for MW

this scans a range of tcpip addresses to find all smart
plugs ( TP Link ) and info about them.

customize in parameters of main() below

Should run in all Python 3.6 and up

Requires the pyHS100 package for the plugs and
the other imports below.

Single threaded version is really slow, this is faster
running in tens of seconds for .25K addresses with 50 workers


To interpret some output you may need to refer to the code.



"""



import concurrent.futures
import time
import os
import psutil

import pyHS100

# --------------------------
class PlugProber( object ):
    """
    accumulate tcpip addresses, then probe for plugs at these
    addresses, finally returning urls that have plugs
    you can set the number of workers, so 250 checks seem
    to take on the order or 30 sec.... test if this is really important
    Use:
        create
        add url ranges or list of ranges
        probe

    """
    #---------
    def __init__(self, max_workers = 50   ):
        """
        Usual init see class doc string
        """
        self.max_workers           = max_workers
        self.plug_list             = []    # plugs found tcpip adr
        self.adr_list              = []    # addresses we will/did look probe for

        # ---- to measure performance
        self.start_mem_mega        = 0
        self.end_mem_mega          = 0
        self.end_time              = 0
        self.start_time            = 0

        self.version               = "Ver 42.0"

    #-----------
    def get_info_str( self ):
        """
        for debug and development -- a poor version of __str__()
        returns a string with info on object
        """
        a_str     =  f"PlugProber {PlugProber}"
        a_str     =  f"{a_str}\nmax_workers     {self.max_workers}"

        a_str     =  f"{a_str}\nstart_time      {self.start_time}"
        a_str     =  f"{a_str}\nend_time        {self.end_time}"
        a_str     =  f"{a_str}\ndelta time      {self.end_time-self.start_time}"

        a_str     =  f"{a_str}\nstart_mem_mega  {self.start_mem_mega}"
        a_str     =  f"{a_str}\nend_mem_mega    {self.end_mem_mega}"
        a_str     =  f"{a_str}\ndelta mem mega  {self.end_mem_mega - self.end_mem_mega}"

        # next may be more info than you want
        #a_str     =  f"{a_str}\nadr_list        {len(self.adr_list)} >> {self.adr_list}"
        a_str     =  f"{a_str}\nplug_list       {len(self.plug_list)} >> {self.plug_list}"

        return a_str

    # ----------------------------------------
    def add_tcpip_list_from_range( self, start_tcpip, start_tcpip_ix, end_tcpip_ix ):
        """
        mutates   self.adr_list adding adrs from range -- the range that will be probed
        add to the self.adr_list from a range
        range looks like  start_tcpip, start_tcpip_ix, end_tcpip_ix
                          "192.168.0.", 0, 122

        return  mutates   self.adr_list
        """
        for ix in range( start_tcpip_ix, end_tcpip_ix ):
            # next two lines could be combined ?? ... then might use list comp.. and list add
            ix       = f"{str(ix):>03}"
            tcpip    = f"{start_tcpip}{ix}"
            self.adr_list.append( tcpip )

    # ----------------------------------------
    def add_range_list( self, a_range_list ):
        """
        add to the adr_list from a list of ranges that will be probed
        for plugs
        a_range_list is a list of ranges like
                             [ ( "192.168.0.", 0,     6 ),
                               ( "192.168.0.", 9,    16 ),
                               ( "192.168.0.", 119, 136 ),
                               ( "192.168.0.", 0,   250 ),  ] # check where called
        mutates   self.adr_list
        """
        for i_range in a_range_list:
            #rint( i_range )
            self.add_tcpip_list_from_range( *i_range )

    # ----------------------------------------
    def scan_a_plug( self, tcpip,  ):
        """
        scan  a single tcpip address for plug

        a plug is anything that responds to the plug protocol
        seems to take on order of 5 sec for each attempt that fails
        return tuple ( boolean_found, tcpip )
        moved into PlugProber as a packaging advantage ... no instance var ??
        """
        ret      = False   # True if plug is found at the tcpip

        try:
            plug            = pyHS100.SmartPlug( tcpip )
            info            = plug.hw_info
            print( f"\nPlug found, info: {info}\n")   # print should probably be removed if
                                                      # code used in app
            ret             = True
        except pyHS100.smartdevice.SmartDeviceException  as exception:
            pass
            #rint( f"{type(exception )} {exception} " )
            #msg         = f"failed to communicate with plug at {tcpip}"
            #rint( msg )
        return ( ret, tcpip )

    # ----------------------------------------
    def probe_list( self ):
        """
        purpose: probe the address list to find live plugs
        args:  none, uses internal state
        return: mutates and returns self.plug_list -> sorted, no dups
        notes:
        ?? could allow passing of list in default None
        ?? should we probe twice in case of network issues?
        need to format all 4 parts not just the last, for now just the last
        """
        # ----- setup
        self.adr_list  = list( set( self.adr_list ) )  # intended to eliminate dups back to list to sort
        self.adr_list.sort()        # sort is lexical, need 3 digits in final section of url

        ret                 = []
        process             = psutil.Process(os.getpid())
        self.start_mem_mega = ( process.memory_info().rss ) / 1_000_000
        self.start_time     = time.time()
        print( "About to start thread pool..." ) # another print to remove...later

        with concurrent.futures.ThreadPoolExecutor( max_workers = self.max_workers ) as executor:

            future_to_url = {executor.submit( self.scan_a_plug, url,  ): url for url in self.adr_list}   #

            for future in concurrent.futures.as_completed( future_to_url ):
                url = future_to_url[future]
                try:
                    data = future.result()
                except Exception as exc:
                    pass # exception used to implie non existance of plug
                    # ?? is this true or should we retest? ... tests seem to show no retry req.
                    #rint('%r generated an exception: %s' % (url, exc))
                else:
                    #rint( f"data returned: {data}"    )
                    if data[0]:
                        ret.append( data[1] )

        self.end_time     = time.time()

        process           = psutil.Process(os.getpid())
        self.end_mem_mega = ( process.memory_info().rss ) / 1_000_000

        ret              = list( set( ret ))  # dups should already be eliminated but for sure
        ret.sort()
        self.plug_list   = ret
        return self.plug_list



# ----------------------------------------
def main():
    print( """\n
    Hi Matthias Wandel ... this is info on smart plugs.....
    """ )

    # ====== parameters - set to your preferences

    # this range is a more complicated example test several discrete adr. ranges
    tcpip_range_list = ([ ( "192.168.0.", 0,     6 ),
                          ( "192.168.0.", 9,    16 ),
                          ( "192.168.0.", 119, 136 ),
                          ( "192.168.0.", 0,   250 ),   ] )

    # this range is a just do "everything" assumes your network is old 192.168.....
    tcpip_range_list  =  ([ ( "192.168.0.", 0,   256 ), ] )

    # # this range is a more complicated example test several discrete adr. ranges
    # tcpip_range_list = ([ ( "192.168.0.", 0,     6 ),
    #                       ( "192.168.0.", 9,    16 ),
    #                       ( "192.168.0.", 119, 136 ),
    #                       ( "192.168.0.", 0,   250 ),   ] )

    number_of_threads =   50       # 50 is default, 1 would be slow how about 200 ?

    # ================= end parameters

    # ------------------ newer test, list of ranges
    print( "\nBuild list of ranges..." )
    a_prober    = PlugProber( max_workers = number_of_threads )

    # this is a list of tuples for the ranges of tcpip addresses to probe
    # in results dups will be droped and results sorted
    a_prober.add_range_list ( tcpip_range_list  )

    print( f"PlugProber info str: {a_prober.get_info_str()}")
    #rint( a_prober )
    print( "\nProbe.... this may take another 30 seconds for final results..." )
    live_plugs   = a_prober.probe_list()

    print( f"\n\nLive_plugs at: {live_plugs}")

    print( f"\n\nPlugProber info str final: {a_prober.get_info_str()}")


if __name__ == "__main__":

    main()







